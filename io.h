#ifndef IO_H
#define IO_H

#include <string>
#include <vector>

#include "gf2n.h"
#include "zp.h"
#include "matrix.h"
#include "vector.h"
#include "polynom.h"
#include "mathFunctions.h"
#include "utility.h"

#define STARTEND "################################################################"

std::vector<long> input(std::vector<std::string> variables, std::string title = "");

std::vector<Zp> inputZp(std::vector<std::string> variables, std::string title = "");

std::vector<GF2n> inputGF2n(std::vector<std::string> variables, std::string title = "");

std::vector<Polynom<Zp>> inputPolynomZp(std::vector<std::string> variables, std::string title = "");

Matrix<Zp> inputMatrixZp(std::string title = "");

Vector<Zp> inputVectorZp(std::string title = "");

Matrix<GF2n> inputMatrixGF2n(std::string title = "");

Vector<GF2n> inputVectorGF2n(std::string title = "");

unsigned int inputSelection(std::string options[], size_t size, std::string title = "");

template<typename T>
void outputResult(const T result, std::string title = "Result") {
	std::cout << title << ": " << std::boolalpha << result << std::endl;
}

template<typename T>
void outputResult(const EuklidResults<T> result, std::string title = "Result") {
	if (title.empty()) {
		std::cout << std::string(STARTEND) << std::endl;
	} else {
		std::cout << std::string(STARTEND).replace(2, title.length() + 2, " " + title + " ") << std::endl;
	}
	std::cout << "#" << std::endl;

	std::cout << "# ggt: " << result.ggt << std::endl;
	std::cout << "# x: " << result.x << std::endl;
	std::cout << "# y: " << result.y << std::endl;

	std::cout << "#" << std::endl;
	std::cout << STARTEND << std::endl << std::endl;
}

#endif //IO_H
