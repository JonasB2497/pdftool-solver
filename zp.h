#ifndef ZP_H
#define ZP_H
#define DATATYPE long

#include <stdexcept>
#include <limits>

class Zp {
private:
	DATATYPE value;
	DATATYPE p;
	bool pUndefined;

	void alignP(const Zp& other);

public:
	Zp() : value(0), p(std::numeric_limits<DATATYPE>::max()), pUndefined(true) {}
	Zp(DATATYPE value, DATATYPE p) : value(value % p), p(p), pUndefined(false) {
		this->pUndefined = (this->p == 0 || this->p == std::numeric_limits<DATATYPE>::max());
	}
	Zp(DATATYPE value) : value(value), p(std::numeric_limits<DATATYPE>::max()), pUndefined(true) {}
	Zp(const Zp& obj) : value(obj.value), p(obj.p), pUndefined(obj.pUndefined) {}
	DATATYPE getValue() const { return this->value; }
	DATATYPE getP() const { return this->p; }

	Zp multiplicativeInvers() const;
	Zp additiveInvers() const;

	Zp& operator=(const Zp& other);
	Zp& operator=(const DATATYPE& other);
	Zp& operator+=(const Zp& other);
	Zp& operator+=(const DATATYPE& other);
	Zp& operator-=(const Zp& other);
	Zp& operator-=(const DATATYPE& other);
	Zp& operator*=(const Zp& other);
	Zp& operator*=(const DATATYPE& other);
	Zp& operator/=(const Zp& other);
	Zp& operator/=(const DATATYPE& other);
	Zp operator+(const Zp& other) const;
	Zp operator+(const DATATYPE& other) const;
	Zp operator-(const Zp& other) const;
	Zp operator-(const DATATYPE& other) const;
	Zp operator*(const Zp& other) const;
	Zp operator*(const DATATYPE& other) const;
	Zp operator/(const Zp& other) const;
	Zp operator/(const DATATYPE& other) const;
	Zp operator-() const;

	bool operator==(const Zp& other) const;
	bool operator==(const DATATYPE& other) const;
	bool operator!=(const Zp& other) const;
	bool operator!=(const DATATYPE& other) const;
	bool operator>(const Zp& other) const;
	bool operator>(const DATATYPE& other) const;
	bool operator<(const Zp& other) const;
	bool operator<(const DATATYPE& other) const;
	bool operator>=(const Zp& other) const;
	bool operator>=(const DATATYPE& other) const;
	bool operator<=(const Zp& other) const;
	bool operator<=(const DATATYPE& other) const;
};

#endif // ZP_H
