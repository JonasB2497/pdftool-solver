#include "io.h"
#include "mathFunctions.h"
#include "utility.h"
#include "zp.h"
#include "ellipticCurve.h"
#include "polynom.h"
#include "utility.h"

#include "spdlog/spdlog.h"

#include <map>
#include <string>
#include <functional>
#include <stdexcept>
#include <vector>
#include <iostream>

/**
* Aufgabe 38
* Addieren in Z_p
*/
void pdf_add() {
	auto results = inputZp({"a", "b"}, "Addieren in Z_p");
	outputResult(results[0] + results[1]);
}

/**
* Aufgabe 39
* Multiplizieren in Z_p
* Info: bei großen Zahlen, die die Größe eines Longs überschreiten wird eine andere Implementierung benötigt
*/
void pdf_multiply() {
	auto results = inputZp({"a", "b"}, "Multiplizieren in Z_p");
	outputResult(results[0] * results[1]);
}

/**
* Aufgabe 40
* Invertieren in Z_p für kleine p
* Info: Unter Umständen könnte diese Suche sehr lange dauern
*/
void pdf_invers() {
	auto results = inputZp({"a"}, "Invertieren in Z_p");
	outputResult(results[0].additiveInvers(), "additiv Inverse");
	outputResult(results[0].multiplicativeInvers(), "multiplicative Inverse");
}

/**
* Aufgabe 41
* Primitive Wurzeln in Z_p
*/
void pdf_primitiveRoot() {
	auto results = input({"a", "p"}, "Primitive Wurzeln in Z_p");
	outputResult((bool)isPrimitiveRoot(results[0], results[1]));
}

/**
* Aufgabe 42
* Periodenfindung in Z^*_p
*/
void pdf_periodfind() {
	auto results = input({"a", "n"}, "Periodenfindung in Z^*_p");
	outputResult(order(results[0], results[1]));
}

/**
* Aufgabe 43
* Matrizenmultiplikation in Z_p
*/
void pdf_matrixmultiplicationZp() {
	auto matrix1 = inputMatrixZp("Matrix 1 zur Multiplikation");
	auto matrix2 = inputMatrixZp("Matrix 2 zur Multiplikation");
	matrix1 *= matrix2;
	outputResult(matrix1);
}

/**
* Aufgabe 44
* Determinanten in Z_p
*/
void pdf_determinantZp() {
	auto matrix = inputMatrixZp("Matrix zur Determinantenberechnung");
	outputResult(matrix.determinant());
}

/**
* Aufgabe 45
* Matrizeninversion in Z_p
*/
void pdf_matrixinversionZp() {
	auto matrix = inputMatrixZp("Matrix zum Invertieren");
	matrix.invert(Zp(0, matrix.get(0,0).getP()), Zp(1, matrix.get(0,0).getP()));
	outputResult(matrix);
}

/**
* Aufgabe 46
* Gleichungssysteme in Z_p
*/
void pdf_linearSystem() {
	auto matrix = inputMatrixZp("Gleichungssystem");
	auto vector = inputVectorZp("Ergebnisvektor");
	outputResult(matrix.linearSystem(vector));
}

/**
* Aufgabe 47
* Eulersche Phi-Funktion
*/
void pdf_eulersche_phifunction() {
	auto results = input({"n"}, "Eulersche Phi-Funktion");
	outputResult(phi(results[0]));
}

/**
* Aufgabe 48
* Euklidischer Algorithmus
*/
void pdf_euklid_algorithmus() {
	auto results = input({"m", "n"}, "Euklidischer Algorithmus");
	outputResult(euklid(results[0], results[1]));
}

/**
* Aufgabe 49
* Invertieren in Z_p für große p
*/
void pdf_biginvers() {
	auto results = inputZp({"a"}, "Invertieren in Z_p für große p");

	outputResult(results[0].additiveInvers(), "additiv Inverse");
	outputResult(results[0].multiplicativeInvers(), "multiplativ Inverse");
}

/**
* Aufgabe 50
* Chinesischer Restsatz
*/
void pdf_chineseRemainderTheorem() {
	long length;
	std::cout << "Values: ";
	std::cin >> length;
	std::vector<std::string> variables;
	// fill the vector with needed variables
	for (long i = 0; i < length; i++) {
		variables.emplace(variables.begin(), "m" + std::to_string(length - i));
		variables.emplace_back("a" + std::to_string(i + 1));
	}

	auto results = input(variables, "Chinesischer Restsatz");
	outputResult(chineseRemainderTheorem(&results[0] + length, &results[0], length));
}

/**
* Aufgabe 51
* Addieren in GF(2^n)
*/
void pdf_addGF() {
	auto results = inputGF2n({"p", "q"}, "Addieren in GF(2^n)");
	outputResult(results[0] + results[1]);
}

/**
* Aufgabe 52
* Multiplizieren in GF(2^n)
*/
void pdf_multiplyGF() {
	auto results = inputGF2n({"p", "q"}, "Multiplizieren in GF(2^n)");
	outputResult(results[0] * results[1]);
}

/**
* Aufgabe 53
* Invertieren in GF(2^n)
*/
void pdf_inversGF() {
	auto results = inputGF2n({"n"}, "Invertieren in GF(2^n)");

	outputResult(results[0].additiveInvers(), "Additiv invers");
	outputResult(results[0].multiplicativeInvers(), "Multiplicative invers");
}

/**
* Aufgabe 54
* Matrizenmultiplikation in GF(2^n)
*/
void pdf_matrixmultiplicationGF2n() {
	auto matrix1 = inputMatrixGF2n("Matrix 1 zur Multiplikation");
	auto matrix2 = inputMatrixGF2n("Matrix 2 zur Multiplikation");
	matrix1 *= matrix2;
	outputResult(matrix1);
}

/**
* Aufgabe 55
* Determinanten in GF(2^n)
*/
void pdf_determinantGF2n() {
	auto matrix = inputMatrixGF2n("Matrix zur Determinantenberechnung");
	outputResult(matrix.determinant());
}

/**
* Aufgabe 56
* Matrizeninversion in GF(2^n)
*/
void pdf_matrixinversionGF2n() {
	auto matrix = inputMatrixGF2n("Matrix zum Invertieren");
	matrix.invert(GF2n(0, matrix.get(0,0).getN(), matrix.get(0,0).getIrreducible()), GF2n(1, matrix.get(0,0).getN(), matrix.get(0,0).getIrreducible()));
	outputResult(matrix);
}

/**
* Aufgabe 57
* Gleichungssysteme in GF(2^n)
*/
void pdf_linearSystemGF2n() {
	auto matrix = inputMatrixGF2n("Matrix");
	auto vector = inputVectorGF2n("Ergebnisvektor");
	outputResult(matrix.linearSystem(vector));
}

/**
* Aufgabe 58
* Diffie-Hellman-Verfahren
*/
void pdf_diffiehellman() {
	auto results = input({"p", "g", "a", "b"}, "Diffie Hellman");

	std::cout << "g^a mod p (Alice): " << quickpot(results[1], results[2], results[0]) << std::endl;
	std::cout << "g^b mod p (Bob): " << quickpot(results[1], results[3], results[0]) << std::endl;
	std::cout << "(g^b)^a mod p (Alice): " << quickpot(quickpot(results[1], results[3], results[0]), results[2], results[0]) << std::endl;
	std::cout << "(g^a)^b mod p (Bob): " << quickpot(quickpot(results[1], results[2], results[0]), results[3], results[0]) << std::endl;
}

/**
* Aufgabe 59
* RSA-Verfahren
*/
void pdf_rsa() {
	auto results = input({"m", "p", "q", "a"}, "RSA");

	long n_tilde = (results[1] - 1) * (results[2] - 1);
	std::cout << "n~ := (p - 1) * (q - 1) = " << n_tilde << std::endl;

	if (ggt(results[3], n_tilde) == 1) {
		std::cout << "ggt(a, n~) == 1" << std::endl;
		std::cout << "\t--> can calculate rsa" << std::endl;

		long b = euklid(results[3], n_tilde).y;
		long n = results[1] * results[2];
		std::cout << "b = " << b << std::endl;
		std::cout << "n := pq = " << n << std::endl;

		if (results[0] > 1 && results[0] < n) {
			std::cout << "1 < m < n" << std::endl;
			long c = quickpot(results[0], b, n);
			std::cout << "c := m^b mod n = " << c << std::endl;
			long m = quickpot(c, results[3], n);
			std::cout << "m = c^a mod n = " << m << std::endl;
		} else {
			std::cout << "1 < m < n not given" << std::endl;
		}
	} else {
		std::cout << "ggt(a, n~) != 1" << std::endl;
		std::cout << "\t--> cannot calculate rsa" << std::endl;
	}
}

/**
* Aufgabe 60
* Elliptische Kurven über Z_p mit p > 3
*/
void pdf_ellipticCurveZp() {
	auto results = input({"p", "a", "b"}, "Elliptische Kurven über Z_p mit p > 3");

	EllipticCurveK3 ec(results[0], results[1], results[2]);
	for (auto point : ec.getPoints()) {
		std::cout << point << ", ";
	}
	std::cout << std::endl << std::endl;

	printEcPlusTable(ec);
}

/**
* Aufgabe 61
* Elliptische Kurven über GF(2^n) mit n > 0
*/
void pdf_ellipticCurveGF2n() {
	auto results = inputGF2n({"a", "b"}, "Elliptische Kurven über GF(2^n) mit n > 0");

	EllipticCurveK2 ec(results[0].getN(), results[0].getIrreducible(), results[0], results[1]);
	std::cout << "Punkte auf der Kurve (" << ec.getPoints().size() << "): " << std::endl << "\t";
	for (auto point : ec.getPoints()) {
		std::cout << point << ", ";
	}
	std::cout << std::endl << std::endl;

	printEcPlusTable(ec);
}

/**
* Aufgabe 62
* Polynommultiplikation über Z_p
*/
void pdf_polynomMultiplicationZp() {
	auto results = inputPolynomZp({"p(X)", "q(X)"}, "Polynommultiplikation über Z_p");

	std::cout << (results[0] * results[1]) << std::endl;
}

/**
* Aufgabe 63
* Polinomdivision über Z_p
*/
void pdf_polynomDivisionZp() {
	auto results = inputPolynomZp({"p(X)", "q(X)"}, "Polynomdivision über Z_p");

	std::cout << (results[0] / results[1]) << std::endl;
}

/**
* Aufgabe 64
* Polynomfaktorisierung über Z_p
*/
void pdf_polynomfactorizingZp() {
	auto results = inputPolynomZp({"p(X)"}, "Polynomfaktorisierung über Z_p");
	auto factors = results[0].factorize();
	for (auto i : factors) {
		std::cout << "(" << i << ")";
	}
	std::cout << std::endl;
}

/**
* Aufgabe 65
* Euklidischer Algorithmus für Polynome über Z_p
*/
void pdf_euklidZp() {
	auto results = inputPolynomZp({"p(X)", "q(X)"}, "Euklidischer Algorithmus für Polynome über Z_p");
	auto values = euklid(results[0], results[1]);
	outputResult(values);
}

/**
* Aufgabe 66
* Polynominvertierung über Z_p
*/
void pdf_polynomInversion() {
	auto results = inputPolynomZp({"p(X)", "d(X)"}, "Polynominvertierung über Z_p");
	std::cout << multiplicativeInvers(results[0], results[1]) << std::endl;
}


void tool_exit() {
	exit(0);
}

int main(int argc, char* argv[]) {

	for (int i = 1; i < argc; i++) {
		if (std::string(argv[i]) == "-v") {
			spdlog::set_level(spdlog::level::debug);
		}
	}

	//Polynom<Zp> poly1({Zp(1, 3), Zp(1, 3),Zp(0, 3),Zp(2, 3),Zp(2, 3)});
	//Polynom<Zp> poly2({Zp(1, 3), Zp(1, 3),Zp(1, 3),Zp(1, 3),Zp(2, 3)});
	//Polynom<Zp> poly1({Zp(4, 7), Zp(2, 7),Zp(4, 7),Zp(5, 7),Zp(6, 7),Zp(5, 7)});
	//Polynom<Zp> poly2({Zp(0, 7), Zp(0, 7),Zp(2, 7),Zp(4, 7),Zp(6, 7)});
	//Polynom<Zp> poly1({Zp(4, 5), Zp(1, 5),Zp(0, 5),Zp(2, 5),Zp(3, 5)});
	//Polynom<Zp> poly2({Zp(1, 5), Zp(2, 5),Zp(2, 5)});
	//Polynom<Zp> poly1({Zp(2, 3), Zp(1, 3),Zp(0, 3),Zp(0, 3),Zp(2, 3)});
	//Polynom<Zp> poly2({Zp(2, 3), Zp(0, 3),Zp(0, 3),Zp(0, 3),Zp(0, 3),Zp(1, 3)});
	//Polynom<Zp> poly1({Zp(2, 5), Zp(1, 5),Zp(4, 5),Zp(3, 5),Zp(4, 5),Zp(1,5)});
	//Polynom<Zp> poly2({Zp(1, 5), Zp(0, 5),Zp(2, 5),Zp(0, 5),Zp(3,5)});
	//Polynom<Zp> poly1({Zp(1, 11), Zp(8, 11),Zp(9, 11),Zp(8, 11),Zp(1, 11)});
	//Polynom<Zp> poly2({Zp(1, 11), Zp(8, 11),Zp(8, 11),Zp(1, 11)});
	//Polynom<Zp> poly1({Zp(6, 7), Zp(1, 7),Zp(0, 7),Zp(0, 7),Zp(6, 7)});
	//Polynom<Zp> poly2({Zp(6, 7), Zp(0, 7),Zp(0, 7),Zp(0, 7),Zp(0, 7),Zp(1,7)});


	//std::cout << ggt(poly1, poly2) << std::endl;
	//std::cout << "test: " << poly1 / ggt(poly1, poly2) << std::endl;
	//std::cout << "test: " << poly2 / ggt(poly1, poly2) << std::endl;
	//std::cout << poly1 / poly2 << std::endl;
	//auto results = euklid(poly1, poly2);
	//outputResult(results);
	//std::cout << multiplicativeInvers(poly1, poly2) << std::endl;

	//return 0;

	std::vector<std::string> names = {
		"Addieren in Z_p",
		"Multiplizieren in Z_p",
		"Invertieren in Z_p für kleine p",
		"Primitive Wurzeln in Z_p",
		"Periodenfindung in Z^*_n",
		"Matrixmultiplikation in Z_p",
		"Determinanten in Z_p",
		"Matrizeninversion in Z_p",
		"Gleichungssysteme in Z_p",
		"Eulersche Phi-Funktion",
		"Euklidischer Algorithmus",
		"Invertieren in Z_p für große p",
		"Chinesischer Restsatz",
		"Addieren in GF(2^n)",
		"Multiplizieren in GF(2^n)",
		"Invertieren in GF(2^n)",
		"Matrizenmultiplikation in GF(2^n)",
		"Determinanten in GF(2^n)",
		"Matrizeninversion in GF(2^n)",
		"Gleichungssysteme in GF(2^n)",
		"Diffie-Hellman-Verfahren",
		"RSA-Verfahren",
		"Elliptische Kurven über Z_p mit p > 3",
		"Elliptische Kurven über GF(2^n) mit n > 0",
		"Polynommultiplikation über Z_p",
		"Polynomdivision über Z_p",
		"Polynomfaktorisierung über Z_p",
		"Euklidischer Algorithmus für Polynome über Z_p",
		"Polynominvertierung über Z_p",
		"EXIT"
	};

	std::vector<std::function<void(void)>> functions = {
		&pdf_add,
		&pdf_multiply,
		&pdf_invers,
		&pdf_primitiveRoot,
		&pdf_periodfind,
		&pdf_matrixmultiplicationZp,
		&pdf_determinantZp,
		&pdf_matrixinversionZp,
		&pdf_linearSystem,
		&pdf_eulersche_phifunction,
		&pdf_euklid_algorithmus,
		&pdf_biginvers,
		&pdf_chineseRemainderTheorem,
		&pdf_addGF,
		&pdf_multiplyGF,
		&pdf_inversGF,
		&pdf_matrixmultiplicationGF2n,
		&pdf_determinantGF2n,
		&pdf_matrixinversionGF2n,
		&pdf_linearSystemGF2n,
		&pdf_diffiehellman,
		&pdf_rsa,
		&pdf_ellipticCurveZp,
		&pdf_ellipticCurveGF2n,
		&pdf_polynomMultiplicationZp,
		&pdf_polynomDivisionZp,
		&pdf_polynomfactorizingZp,
		&pdf_euklidZp,
		&pdf_polynomInversion,
		&tool_exit
	};

	while(true) {
		if (names.size() != functions.size()) {
			throw std::runtime_error("vectors not of equal size (" + std::to_string(names.size()) + ", " + std::to_string(functions.size()) + ")");
		}
		int result = inputSelection(names.data(), names.size());
		try {
			functions.at(result)();
		} catch (std::runtime_error& e) {
			std::cout << e.what() << std::endl;
		}

		std::cout << "--- press enter to continue ---";
		std::cin.ignore();
		std::cin.get();
	}

	return 0;
}
