#ifndef MATHFUNCTIONS_H
#define MATHFUNCTIONS_H

#include "polynom.h"
#include "zp.h"
#include "spdlog/spdlog.h"
#include "spdlog/fmt/ostr.h"
#include "spdlog/fmt/bundled/ranges.h"

#include <vector>
#include <sstream>

template<typename T>
struct EuklidResults {
	T ggt;
	T x;
	T y;
};

long quickpot(long a, long b, long p);
long order(long a, long p);
template<typename T>
	T ggt(T a, T b);
long phi(long n);
bool isPrimitiveRoot(long a, long n);

template<typename T>
	EuklidResults<T> euklid(T m, T n);
long chineseRemainderTheorem(long* a, long* m, std::size_t len);

template<typename T>
Polynom<T> multiplicativeInvers(Polynom<T> p, Polynom<T> d) {
	auto results = euklid(p, d);
	if (results.ggt.getDegree() > 0) {
		std::stringstream ss;
		ss << "x is not constant (";
		ss << results.ggt;
		ss << ") -> invers not found";
		throw std::runtime_error(ss.str());
	}
	spdlog::debug("t(X) = {}", results.ggt);
	// we can get the multiplicative inverse of the ggt polynom here like this because the if clause above catches all polynoms with a degree > 0
	results.ggt.setMultiplicatorAt(0, results.ggt.getMultiplicatorAt(0).multiplicativeInvers());
	return results.ggt * results.x;
}

#endif // MATHFUNCTIONS_H
