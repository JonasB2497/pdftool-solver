#include "gf2n.h"

#include <stdexcept>
#include <math.h>

void GF2n::align(const GF2n& other) {
	if (this->n == 0 && other.n == 0) {
		throw std::runtime_error("n is invalid");
	}
	else if (this->n != 0 && other.n == 0) {
		// no problem but we should not change the other object
	}
	else if (this->n == 0 && other.n != 0) {
		this->n = other.n;
	}
	else if (this->n != other.n) {
		throw std::runtime_error("n not equal (" + std::to_string(this->n) + ", " + std::to_string(other.n) + ")");
	}

	// TODO: is this check for irreducibles correct?
	if (this->irreducible == 0 && other.irreducible == 0) {
		throw std::runtime_error("irreducible is invalid");
	}
	else if (this->irreducible != 0 && other.irreducible == 0) {
		// no problem but we should not change the other object
	}
	else if (this->irreducible == 0 && other.irreducible != 0) {
		this->irreducible = other.irreducible;
	}
	else if (this->irreducible != other.irreducible) {
		throw std::runtime_error("irreducible not equal (" + std::to_string(this->irreducible) + ", " + std::to_string(other.irreducible) + ")");
	}
}

GF2n GF2n::multiplicativeInvers() const {
	// TODO: this can be done better with euklid
	for (int j = 1; j < pow(2, this->n); j++) {
		if ((*this * j) == 1) {
			return GF2n(j, this->n, this->irreducible);
		}
	}
	throw std::runtime_error("no inverse in GF found");
}

GF2n GF2n::additiveInvers() const {
	return GF2n(*this);
}



GF2n& GF2n::operator=(const GF2n& other) {
	this->value = other.value;
	this->n = other.n;
	this->irreducible = other.irreducible;
	return *this;
}

GF2n& GF2n::operator=(const DATATYPE& other) {
	this->value = other;
	return *this;
}

GF2n& GF2n::operator+=(const GF2n& other) {
	this->align(other);
	// xor
	this->value ^= other.value;
	return *this;
}

GF2n& GF2n::operator+=(const DATATYPE& other) {
	this->value ^= other;
	return *this;
}

GF2n& GF2n::operator-=(const GF2n& other) {
	*this += other.additiveInvers();
	return *this;
}

GF2n& GF2n::operator-=(const DATATYPE& other) {
	*this += GF2n(other).additiveInvers();
	return *this;
}

GF2n& GF2n::operator*=(const GF2n& other) {
	this->align(other);

	/*long rangeMax = pow(2, n) - 1;
	if (p > rangeMax || q > rangeMax || i > pow(2, n+1) - 1) {
		throw std::runtime_error("p, q or i to big");
	}*/

	long result = 0;
	for (long j = 0; j < n; j++) {
		long index = pow(2, j);
		result ^= ((long)((this->value >> j) % 2)) * (other.value * index);
	}

	// reduce by irreducible polynom
	for (long j = this->n; j >= 0; j--) {
		long index = pow(2, j);
		if (floor(log2(index * this->irreducible)) <= floor(log2(result))) {
			// irreducible polynom "fits" in result -> addGF2n
			result ^= (index * this->irreducible);
		}
	}

	this->value = result;

	return *this;
}

GF2n& GF2n::operator*=(const DATATYPE& other) {
	*this *= GF2n(other, this->n, this->irreducible);
	return *this;
}

GF2n& GF2n::operator/=(const GF2n& other) {
	(*this) *= other.multiplicativeInvers();
	return *this;
}

GF2n& GF2n::operator/=(const DATATYPE& other) {
	(*this) /= GF2n(other, this->n, this->irreducible);
	return *this;
}

GF2n GF2n::operator+(const GF2n& other) const {
	GF2n newGF2n(*this);
	return newGF2n += other;
}

GF2n GF2n::operator+(const DATATYPE& other) const {
	GF2n newGF2n(*this);
	return newGF2n += other;
}

GF2n GF2n::operator-(const GF2n& other) const {
	GF2n newGF2n(*this);
	return newGF2n -= other;
}

GF2n GF2n::operator-(const DATATYPE& other) const {
	GF2n newGF2n(*this);
	return newGF2n -= other;
}

GF2n GF2n::operator*(const GF2n& other) const {
	GF2n newGF2n(*this);
	return newGF2n *= other;
}

GF2n GF2n::operator*(const DATATYPE& other) const {
	GF2n newGF2n(*this);
	return newGF2n *= other;
}

GF2n GF2n::operator/(const GF2n& other) const {
	GF2n newGF2n(*this);
	return newGF2n /= other;
}

GF2n GF2n::operator/(const DATATYPE& other) const {
	GF2n newGF2n(*this);
	return newGF2n /= other;
}

GF2n GF2n::operator-() const {
	return this->additiveInvers();
}



bool GF2n::operator==(const GF2n& other) const {
	return (this->value == other.value) && (this->n == other.n) && (this->irreducible == other.irreducible);
}

bool GF2n::operator==(const DATATYPE& other) const {
	return (this->value == other);
}

bool GF2n::operator!=(const GF2n& other) const {
	return !(*this == other);
}

bool GF2n::operator!=(const DATATYPE& other) const {
	return !(*this == other);
}

bool GF2n::operator>(const GF2n& other) const {
	return this->value > other.value;
}

bool GF2n::operator>(const DATATYPE& other) const {
	return this->value > other;
}

bool GF2n::operator<(const GF2n& other) const {
	return this->value < other.value;
}

bool GF2n::operator<(const DATATYPE& other) const {
	return this->value < other;
}

bool GF2n::operator>=(const GF2n& other) const {
	return this->value >= other.value;
}

bool GF2n::operator>=(const DATATYPE& other) const {
	return this->value >= other;
}

bool GF2n::operator<=(const GF2n& other) const {
	return this->value <= other.value;
}

bool GF2n::operator<=(const DATATYPE& other) const {
	return this->value <= other;
}
