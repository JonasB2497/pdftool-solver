#ifndef VECTOR_H
#define VECTOR_H

#include <vector>

template<typename T>
class Vector : public std::vector<T> {
public:
	using std::vector<T>::vector;
};

#endif
