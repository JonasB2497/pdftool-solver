#ifndef POLYNOM_H
#define POLYNOM_H

#include <iostream>
#include <math.h>
#include <stdexcept>
#include <list>
#include <vector>



template<typename T>
class Polynom {
private:
	Polynom<T>* remainder;
	std::vector<T> components;

	void reduce() {
		// a polynom has at least one element (x^0)
		while (this->components.size() > 1) {
			if (this->components.back() == T()) {
				this->components.pop_back();
			} else {
				break;
			}
		}
	}

	void factorize(std::list<Polynom<T>>& factors, Polynom<T> poly) {
		// TODO: this only works for Zp

		if (poly.getDegree() == 0) {
			return;
		}

		Polynom<T> factor(1);
		T multiplicator = poly.getMultiplicatorAt(poly.getDegree()) / poly.getMultiplicatorAt(poly.getDegree());
		factor.setMultiplicatorAt(1, multiplicator);
		multiplicator *= 0;
		factor.setMultiplicatorAt(0, multiplicator);

		do {
			auto result = poly / factor;
			if (!result.hasRemainder()) {
				// found a fitting factor
				result.factorize(factors, result);
				factors.emplace_back(factor);
				return;
			}
			factor.setMultiplicatorAt(0, factor.getMultiplicatorAt(0) + 1);
		} while (factor.getMultiplicatorAt(0) != T());

		throw std::runtime_error("no more zero points found");
	}

public:
	Polynom() : Polynom({0}) {}
	Polynom(std::vector<T> components) : components(components), remainder(nullptr) {this->reduce();}
	Polynom(const Polynom<T>& p) : components(p.components) {
		if (p.remainder != nullptr) {
			this->remainder = new Polynom<T>(*p.remainder);
		} else {
			this->remainder = nullptr;
		}
		//this->reduce();
	}
	Polynom(long degree) : remainder(nullptr) {
		if (degree < 0) {
			throw std::runtime_error("degree of polynom negative (" + std::to_string(degree) + ")");
		}
		for (long i = 0; i <= degree; i++) {
			this->components.emplace_back(T());
		}
	}

	~Polynom() {
		if (this->remainder != nullptr) {
			delete this->remainder;
		}
	}

	// note: not implemented here but a polynom with only 0-Coefficients should have a degree of -Infinity (in this implementation: -1)
	long getDegree() const { return this->components.size() - 1; }

	bool hasRemainder() const {
		return this->remainder != nullptr && (this->remainder->getDegree() != 0 || this->remainder->getMultiplicatorAt(0) != T());
	}

	Polynom<T> getRemainder() const {
		if (this->remainder == nullptr) {
			return Polynom(0);
		}
		return *this->remainder;
	}

	const T getMultiplicatorAt(long index) const { return this->components.at(index); }

	void setMultiplicatorAt(long index, const T multiplicator) { this->components[index] = multiplicator; }

	std::list<Polynom<T>> factorize() {
		std::list<Polynom<T>> factors;
		this->factorize(factors, (*this));
		return factors;
	}

	Polynom<T>& operator=(const Polynom<T>& other) {
		this->components = other.components;
		return (*this);
	}
	Polynom<T>& operator+=(const Polynom<T>& other) {
		// find the maximum exponent
		long maxLength = std::max(this->components.size(), other.components.size());

		for (long i = 0; i < maxLength; i++) {
			if (this->components.size() <= i) {
				// add a new element to accomodate the bigger other polynom
				this->components.emplace_back(T());
			}
			if (other.components.size() <= i) {
				// the other polynom does not have more elements. We can stop here
				break;
			}
			this->components.at(i) += other.components.at(i);
		}
		this->reduce();
		return (*this);
	}
	Polynom<T>& operator-=(const Polynom<T>& other) {
		// find the maximum exponent
		long maxLength = std::max(this->components.size(), other.components.size());

		for (long i = 0; i < maxLength; i++) {
			if (this->components.size() <= i) {
				// add a new element to accomodate the bigger other polynom
				this->components.emplace_back(T());
			}
			if (other.components.size() <= i) {
				// the other polynom does not have more elements. We can stop here
				break;
			}
			this->components.at(i) -= other.components.at(i);
		}
		this->reduce();
		return (*this);
	}
	Polynom<T>& operator*=(const Polynom<T>& other) {
		this = (*this) * other;
		return (*this);
	}
	Polynom<T>& operator/=(const Polynom<T>& d) {
		// maybe stolen from https://en.wikipedia.org/wiki/Polynomial_long_division#Pseudocode
		if (d.components.size() == 0) {
			throw std::runtime_error("dividing polynom cannot be 0");
		}

		Polynom<T> q(this->getDegree());
		Polynom<T> r(*this);

		while (!(r.getDegree() == 0 && r.getMultiplicatorAt(0) == 0) && r.getDegree() >= d.getDegree()) {
			T t = r.getMultiplicatorAt(r.getDegree()) / d.getMultiplicatorAt(d.getDegree());
			Polynom<T> tPoly(r.getDegree() - d.getDegree());
			tPoly.components.at(tPoly.getDegree()) = t;
			q += tPoly;
			r -= tPoly * d;
		}

		this->components = q.components;
		this->remainder = new Polynom(r);
		this->reduce();
		return (*this);
	}
	Polynom<T>& operator%=(const Polynom<T>& d) {
		(*this) /= d;
		(*this) = (*this->remainder);
		return (*this);
	}

	Polynom<T> operator+(const Polynom<T>& other) const {
		return Polynom<T>(*this) += other;
	}
	Polynom<T> operator-(const Polynom<T>& other) const {
		return Polynom<T>(*this) -= other;
	}
	Polynom<T> operator*(const Polynom<T>& other) const {
		long maxDegree = this->getDegree() + other.getDegree();
		Polynom<T> p(maxDegree);

		for (long i = 0; i <= maxDegree; i++) {
			for (long j = 0; j <= this->getDegree(); j++) {
				long otherDegree = i - j;
				if ((otherDegree >= 0) && (otherDegree <= other.getDegree())) {
					p.components.at(i) += this->components.at(j) * other.components.at(otherDegree);
				}
			}
		}
		p.reduce();
		return p;
	}
	Polynom<T> operator/(const Polynom<T>& other) const {
		return Polynom<T>(*this) /= other;
	}
	Polynom<T> operator%(const Polynom<T>& other) const {
		return Polynom<T>(*this) %= other;
	}
	Polynom<T> operator-() const {
		Polynom<T> temp(*this);
		for (size_t i = 0; i <= temp.getDegree(); i++) {
			temp.setMultiplicatorAt(i, temp.getMultiplicatorAt(i).additiveInvers());
		}
		return temp;
	}

	bool operator==(const Polynom<T>& other) const {
		if (this->getDegree() != other.getDegree()) {
			return false;
		}
		for (long i = 0; i <= this->getDegree(); i++) {
			if (this->getMultiplicatorAt(i) != other.getMultiplicatorAt(i)) {
				return false;
			}
		}
		return true;
	}
	bool operator!=(const Polynom<T>& other) const {
		return !((*this) == other);
	}
	bool operator>(const Polynom<T>& other) const {
		if (this->getDegree() == other.getDegree()) {
			for (long i = this->getDegree(); i >= 0; i--) {
				if (this->getMultiplicatorAt(i) > other.getMultiplicatorAt(i)) {
					return true;
				} else if (this->getMultiplicatorAt(i) < other.getMultiplicatorAt(i)) {
					return false;
				}
			}
			return false;
		}
		return this->getDegree() > other.getDegree();
	}
	bool operator<(const Polynom<T>& other) const {
		return !((*this > other) || (*this == other));
	}
	bool operator>=(const Polynom<T>& other) const {
		return ((*this > other) || (*this == other));
	}
	bool operator<=(const Polynom<T>& other) const {
		return !(*this > other);
	}
};

#endif // POLYNOM_H
