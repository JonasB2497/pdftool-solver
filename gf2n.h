#ifndef GF2N_H
#define GF2N_H
#define DATATYPE long

class GF2n {
private:
	DATATYPE value;
	DATATYPE n;
	DATATYPE irreducible;

	void align(const GF2n& other);

public:
	GF2n() : GF2n(0) {}
	GF2n(DATATYPE value) : GF2n(value, 0, 0) {}
	GF2n(DATATYPE value, DATATYPE n, DATATYPE irreducible) : value(value), n(n), irreducible(irreducible) {}
	GF2n(const GF2n& obj) : value(obj.value), n(obj.n), irreducible(obj.irreducible) {}

	DATATYPE getValue() const { return this->value; }
	DATATYPE getN() const { return this->n; }
	DATATYPE getIrreducible() const { return this->irreducible; }

	GF2n multiplicativeInvers() const;
	GF2n additiveInvers() const;

	GF2n& operator=(const GF2n& other);
	GF2n& operator=(const DATATYPE& other);
	GF2n& operator+=(const GF2n& other);
	GF2n& operator+=(const DATATYPE& other);
	GF2n& operator-=(const GF2n& other);
	GF2n& operator-=(const DATATYPE& other);
	GF2n& operator*=(const GF2n& other);
	GF2n& operator*=(const DATATYPE& other);
	GF2n& operator/=(const GF2n& other);
	GF2n& operator/=(const DATATYPE& other);
	GF2n operator+(const GF2n& other) const;
	GF2n operator+(const DATATYPE& other) const;
	GF2n operator-(const GF2n& other) const;
	GF2n operator-(const DATATYPE& other) const;
	GF2n operator*(const GF2n& other) const;
	GF2n operator*(const DATATYPE& other) const;
	GF2n operator/(const GF2n& other) const;
	GF2n operator/(const DATATYPE& other) const;
	GF2n operator-() const;

	bool operator==(const GF2n& other) const;
	bool operator==(const DATATYPE& other) const;
	bool operator!=(const GF2n& other) const;
	bool operator!=(const DATATYPE& other) const;
	bool operator>(const GF2n& other) const;
	bool operator>(const DATATYPE& other) const;
	bool operator<(const GF2n& other) const;
	bool operator<(const DATATYPE& other) const;
	bool operator>=(const GF2n& other) const;
	bool operator>=(const DATATYPE& other) const;
	bool operator<=(const GF2n& other) const;
	bool operator<=(const DATATYPE& other) const;

};

#endif
