#include "zp.h"
#include "mathFunctions.h"


void Zp::alignP(const Zp& other) {
	if (this->p == 0 || this->p == std::numeric_limits<DATATYPE>::max()) {
		this->pUndefined = true;
	}
	if (this->p == 0 && other.p == 0) {
		// invalid
		throw std::runtime_error("p is invalid");
	}
	else if (this->pUndefined && other.pUndefined) {
		// this is normally invalid but we should let it pass so calculating with two default constructed Zp works
	}
	else if (!this->pUndefined && other.pUndefined) {
		// no problem but we should not change the other object
	}
	else if (this->pUndefined && !other.pUndefined) {
		this->pUndefined = false;
		this->p = other.p;
	}
	else if (this->p != other.p) {
		// invalid
		throw std::runtime_error("p not equal (" + std::to_string(this->p) + ", " + std::to_string(other.p) + ")");
	}
}

Zp Zp::multiplicativeInvers() const {
	auto solution = euklid(this->value, this->p);
	if (solution.ggt != 1) {
		throw std::runtime_error("no solution found (ggt != 1)");
	}
	return Zp(solution.y, this->p);
}

Zp Zp::additiveInvers() const {
	return Zp(this->p - (this->value % this->p), this->p);
}

Zp& Zp::operator=(const Zp& other) {
	this->value = other.value;
	this->p = other.p;
	this->pUndefined = other.pUndefined;
	return *this;
}

Zp& Zp::operator=(const DATATYPE& other) {
	this->value = other;
	return *this;
}

Zp& Zp::operator+=(const Zp& other) {
	this->alignP(other);
	this->value += other.value;
	this->value %= this->p;
	return *this;
}

Zp& Zp::operator+=(const DATATYPE& other) {
	this->value += other;
	this->value %= this->p;
	return *this;
}

Zp& Zp::operator-=(const Zp& other) {
	*this += other.additiveInvers();
	return *this;
}

Zp& Zp::operator-=(const DATATYPE& other) {
	*this += Zp(other).additiveInvers();
	return *this;
}

Zp& Zp::operator*=(const Zp& other) {
	this->alignP(other);
	this->value *= other.value;
	this->value %= this->p;
	return *this;
}

Zp& Zp::operator*=(const DATATYPE& other) {
	this->value *= other;
	this->value %= this->p;
	return *this;
}

Zp& Zp::operator/=(const Zp& other) {
	(*this) *= other.multiplicativeInvers();
	return *this;
}

Zp& Zp::operator/=(const DATATYPE& other) {
	(*this) /= Zp(other, this->p);
	return *this;
}

Zp Zp::operator+(const Zp& other) const {
	Zp newZp(*this);
	return newZp += other;
}

Zp Zp::operator+(const DATATYPE& other) const {
	Zp newZp(*this);
	return newZp += other;
}

Zp Zp::operator-(const Zp& other) const {
	Zp newZp(*this);
	return newZp -= other;
}

Zp Zp::operator-(const DATATYPE& other) const {
	Zp newZp(*this);
	return newZp -= other;
}

Zp Zp::operator*(const Zp& other) const {
	Zp newZp(*this);
	return newZp *= other;
}

Zp Zp::operator*(const DATATYPE& other) const {
	Zp newZp(*this);
	return newZp *= other;
}

Zp Zp::operator/(const Zp& other) const {
	Zp newZp(*this);
	return newZp /= other;
}

Zp Zp::operator/(const DATATYPE& other) const {
	Zp newZp(*this);
	return newZp /= other;
}

Zp Zp::operator-() const {
	return this->additiveInvers();
}





bool Zp::operator==(const Zp& other) const {
	return (this->value == other.value) && ((this->p == other.p) || this->pUndefined || other.pUndefined);
}

bool Zp::operator==(const DATATYPE& other) const {
	return (this->value == other);
}

bool Zp::operator!=(const Zp& other) const {
	return !(*this == other);
}

bool Zp::operator!=(const DATATYPE& other) const {
	return !(*this == other);
}

bool Zp::operator>(const Zp& other) const {
	return this->value > other.value;
}

bool Zp::operator>(const DATATYPE& other) const {
	return this->value > other;
}

bool Zp::operator<(const Zp& other) const {
	return this->value < other.value;
}

bool Zp::operator<(const DATATYPE& other) const {
	return this->value < other;
}

bool Zp::operator>=(const Zp& other) const {
	return this->value >= other.value;
}

bool Zp::operator>=(const DATATYPE& other) const {
	return this->value >= other;
}

bool Zp::operator<=(const Zp& other) const {
	return this->value <= other.value;
}

bool Zp::operator<=(const DATATYPE& other) const {
	return this->value <= other;
}
