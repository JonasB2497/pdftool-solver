#include "utility.h"

#include <math.h>
#include <iomanip>
#include <string>

long binToDec(long bin) {
	long result = 0;
	long i = ceil(log10(bin));
	while (i >= 0) {
		long index = pow(10, i);
		result += (long)(bin / index) * pow(2, i);
		bin = bin % index;
		i--;
	}
	return result;
}

long decToBin(long dec) {
	long result = 0;
	long i = ceil(log2(dec));
	while (i >= 0) {
		long index = pow(2, i);
		result += (long)(dec / index) * pow(10, i);
		dec = dec % index;
		i--;
	}
	return result;
}

std::ostream& operator<<(std::ostream& os, const GF2n value) {
	os << std::setfill('0') << std::setw(value.getN()) << decToBin(value.getValue());
	return os;
}

std::ostream& operator<<(std::ostream& os, const Zp value) {
	os << value.getValue();
	return os;
}
