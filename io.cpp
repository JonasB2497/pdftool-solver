#include <iostream>
#include <stdexcept>
#include <limits>

#include "io.h"
#include "utility.h"

std::vector<long> input(std::vector<std::string> variables, std::string title) {
	if (title.empty()) {
		std::cout << std::string(STARTEND) << std::endl;
	} else {
		std::cout << std::string(STARTEND).replace(2, title.length() + 2, " " + title + " ") << std::endl;
	}
	std::cout << "#" << std::endl;

	std::vector<long> parameters;
	long temp;

	for (size_t i = 0; i < variables.size(); i++) {
		std::cout << "# " << variables[i] << " = ";
		std::cin >> temp;
		parameters.push_back(temp);
	}

	std::cout << "#" << std::endl;
	std::cout << STARTEND << std::endl << std::endl;

	return parameters;
}

std::vector<Zp> inputZp(std::vector<std::string> variables, std::string title) {
	if (title.empty()) {
		std::cout << std::string(STARTEND) << std::endl;
	} else {
		std::cout << std::string(STARTEND).replace(2, title.length() + 2, " " + title + " ") << std::endl;
	}
	std::cout << "#" << std::endl;

	long p;
	long temp;
	std::vector<Zp> parameters;

	std::cout << "# p = ";
	std::cin >> p;

	for (size_t i = 0; i < variables.size(); i++) {
		std::cout << "# " << variables[i] << " = ";
		std::cin >> temp;
		parameters.push_back(Zp(temp, p));
	}

	std::cout << "#" << std::endl;
	std::cout << STARTEND << std::endl << std::endl;

	return parameters;
}

std::vector<GF2n> inputGF2n(std::vector<std::string> variables, std::string title) {
	if (title.empty()) {
		std::cout << std::string(STARTEND) << std::endl;
	} else {
		std::cout << std::string(STARTEND).replace(2, title.length() + 2, " " + title + " ") << std::endl;
	}
	std::cout << "#" << std::endl;

	long n;
	long irreducible;
	long temp;
	std::vector<GF2n> parameters;

	std::cout << "# n = ";
	std::cin >> n;
	std::cout << "# irreducible = ";
	std::cin >> irreducible;
	irreducible = binToDec(irreducible);


	for (size_t i = 0; i < variables.size(); i++) {
		std::cout << "# " << variables[i] << " = ";
		std::cin >> temp;
		temp = binToDec(temp);
		parameters.push_back(GF2n(temp, n, irreducible));
	}

	std::cout << "#" << std::endl;
	std::cout << STARTEND << std::endl << std::endl;

	return parameters;
}

std::vector<Polynom<Zp>> inputPolynomZp(std::vector<std::string> variables, std::string title) {
	if (title.empty()) {
		std::cout << std::string(STARTEND) << std::endl;
	} else {
		std::cout << std::string(STARTEND).replace(2, title.length() + 2, " " + title + " ") << std::endl;
	}
	std::cout << "#" << std::endl;

	long count;
	long p;
	long temp;
	std::vector<Polynom<Zp>> parameters;

	for (size_t i = 0; i < variables.size(); i++) {
		std::cout << std::string(STARTEND).replace(2, variables[i].length() + 2, " " + variables[i] + " ") << std::endl;

		std::cout << "# max Exponent = ";
		std::cin >> count;
		std::cout << "# p = ";
		std::cin >> p;

		std::vector<Zp> poly;

		for (long j = 0; j <= count; j++) {
			std::cout << "# " << "x^" <<j << " = ";
			std::cin >> temp;
			poly.push_back(Zp(temp, p));
		}

		parameters.push_back(Polynom(poly));
	}

	std::cout << "#" << std::endl;
	std::cout << STARTEND << std::endl << std::endl;

	return parameters;
}

Matrix<Zp> inputMatrixZp(std::string title) {
	if (title.empty()) {
		std::cout << std::string(STARTEND) << std::endl;
	} else {
		std::cout << std::string(STARTEND).replace(2, title.length() + 2, " " + title + " ") << std::endl;
	}
	std::cout << "#" << std::endl;

	long p;
	size_t rows;
	size_t columns;
	long temp;

	std::cout << "# p = ";
	std::cin >> p;
	std::cout << "# rows = ";
	std::cin >> rows;
	std::cout << "# columns = ";
	std::cin >> columns;

	Matrix<Zp> matrix(rows, columns);

	for (size_t i = 0; i < rows; i++) {
		for (size_t j = 0; j < columns; j++) {
			std::cout << "# [" << (i+1) << "][" << (j+1) << "] = ";
			std::cin >> temp;
			matrix.set(i, j, Zp(temp, p));
		}
	}

	std::cout << "#" << std::endl;
	std::cout << STARTEND << std::endl << std::endl;

	return matrix;
}

Vector<Zp> inputVectorZp(std::string title) {
	if (title.empty()) {
		std::cout << std::string(STARTEND) << std::endl;
	} else {
		std::cout << std::string(STARTEND).replace(2, title.length() + 2, " " + title + " ") << std::endl;
	}
	std::cout << "#" << std::endl;

	long p;
	size_t elements;
	long temp;

	std::cout << "# p = ";
	std::cin >> p;
	std::cout << "# elements = ";
	std::cin >> elements;

	Vector<Zp> vector;

	for (size_t i = 0; i < elements; i++) {
		std::cout << "# [" << (i+1) << "] = ";
		std::cin >> temp;
		vector.emplace_back(Zp(temp, p));
	}

	std::cout << "#" << std::endl;
	std::cout << STARTEND << std::endl << std::endl;

	return vector;
}

Matrix<GF2n> inputMatrixGF2n(std::string title) {
	if (title.empty()) {
		std::cout << std::string(STARTEND) << std::endl;
	} else {
		std::cout << std::string(STARTEND).replace(2, title.length() + 2, " " + title + " ") << std::endl;
	}
	std::cout << "#" << std::endl;

	long n;
	long irreducible;
	size_t rows;
	size_t columns;
	long temp;

	std::cout << "# n = ";
	std::cin >> n;
	std::cout << "# irreducible = ";
	std::cin >> irreducible;
	std::cout << "# rows = ";
	std::cin >> rows;
	std::cout << "# columns = ";
	std::cin >> columns;

	irreducible = binToDec(irreducible);

	Matrix<GF2n> matrix(rows, columns);

	for (size_t i = 0; i < rows; i++) {
		for (size_t j = 0; j < columns; j++) {
			std::cout << "# [" << (i+1) << "][" << (j+1) << "] = ";
			std::cin >> temp;
			matrix.set(i, j, GF2n(binToDec(temp), n, irreducible));
		}
	}

	std::cout << "#" << std::endl;
	std::cout << STARTEND << std::endl << std::endl;

	return matrix;
}

Vector<GF2n> inputVectorGF2n(std::string title) {
	if (title.empty()) {
		std::cout << std::string(STARTEND) << std::endl;
	} else {
		std::cout << std::string(STARTEND).replace(2, title.length() + 2, " " + title + " ") << std::endl;
	}
	std::cout << "#" << std::endl;

	long n;
	long irreducible;
	size_t elements;
	long temp;

	std::cout << "# n = ";
	std::cin >> n;
	std::cout << "# irreducible = ";
	std::cin >> irreducible;
	std::cout << "# elements = ";
	std::cin >> elements;

	irreducible = binToDec(irreducible);

	Vector<GF2n> vector;

	for (size_t i = 0; i < elements; i++) {
		std::cout << "# [" << (i+1) << "] = ";
		std::cin >> temp;
		vector.emplace_back(GF2n(binToDec(temp), n, irreducible));
	}

	std::cout << "#" << std::endl;
	std::cout << STARTEND << std::endl << std::endl;

	return vector;
}

unsigned int inputSelection(std::string options[], size_t size, std::string title) {
	if (title.empty()) {
		std::cout << std::string(STARTEND) << std::endl;
	} else {
		std::cout << std::string(STARTEND).replace(2, title.length() + 2, " " + title + " ") << std::endl;
	}
	std::cout << "#" << std::endl;

	for (size_t i = 0; i < size; i++) {
		std::cout << "#  " << (i+1) << ".\t" << options[i] << std::endl;
	}

	std::cout << "#" << std::endl;

	unsigned int result = 0;
	while(true) {
		std::cout << "#  Choice: ";
		std::cin >> result;
		std::cin.clear();
		std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');

		if (result > size || result <= 0) {
			std::cout << "# unknown choice. Please try again" << std::endl;
			//throw std::runtime_error("choice not available: " + std::to_string(result));
		} else {
			break;
		}
	}

	std::cout << "#  Selection: " << options[result - 1] << std::endl;
	std::cout << "#" << std::endl;
	std::cout << STARTEND << std::endl << std::endl;

	return result - 1;
}
