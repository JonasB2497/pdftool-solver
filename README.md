# pdfTool solver

## Description
Ein Solver für das PDF-Tool der Veranstaltung "Mathematische Grundlagen der Verschlüsselungstechnik" von Prof. Lenze an der FH-Dortmund

[Buch Basiswissen Angewandte Mathematik](https://www.amazon.de/gp/product/B08GMB27WQ/ref=dbs_a_def_rwt_hsch_vapi_tkin_p1_i1)

## Installation
No installation needed
```
git clone --recurse-submodules https://gitlab.com/JonasB2497/pdftool-solver.git
cd pdftool-solver
mkdir build
cd build
cmake ../
make
```

## Usage
Just start the solver executable and choose the excercise number you want to solve.

The `-v` flag gives some additional informations

## Contributing
If you want to contribute just make a pull request with your changes and I will include it.

The code you write shall be platform independent.

## Authors and acknowledgment
- me

## License
GNU GENERAL PUBLIC LICENSE

## Project status
As I am done with the module I wrote this code for no new features will be added by me. But I will look forward  to contributions from future students.
