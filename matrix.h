#ifndef MATRIX_H
#define MATRIX_H

#include "vector.h"
#include <stddef.h>
#include <stdexcept>

template<typename T>
class Matrix {
private:
	void clearData() {
		for (size_t i = 0; i < this->rows; i++) {
			delete this->matrix[i];
		}
		delete this->matrix;

		this->rows = 0;
		this->columns = 0;
	}

	void copy(const Matrix<T>& other) {
		this->rows = other.rows;
		this->columns = other.columns;

		this->matrix = new T*[this->rows];
		for (size_t i = 0; i < this->rows; i++) {
			this->matrix[i] = new T[this->columns];
			for (size_t j = 0; j < this->columns; j++) {
				this->matrix[i][j] = other.matrix[i][j];
			}
		}
	}

	void gauss(Matrix<T>& other) {
		if (this->determinant() > 0) {
			// the matrix has to be square for this to work
			if (this->rows != this->columns) {
				throw std::runtime_error("to invert the matrix it has to be square");
			}

			// both matrixes have to have the same number of rows
			if (this->rows != other.rows) {
				throw std::runtime_error("for gauss both matrixes have to have the same number of rows");
			}

			// set the diagonal element in each row to 1 and eliminate every other value in that column
			for (size_t i = 0; i < this->rows; i++) {
				if (this->matrix[i][i] == 0) {
					// the diagonal cannot be 0. fix this by flipping with a row below
					for (size_t j = i; j < this->rows; j++) {
						if (this->matrix[j][i] != 0 && this->matrix[i][j] != 0) {
							// found a match! flip
							this->flipRows(i, j);
							other.flipRows(i, j);
							goto diagCorrect;
						}
					}
					// no other fitting row found
					throw std::runtime_error("the diagonal cannot be not zero. With this simple algorithm this does not work");
				}
				diagCorrect:


				T mulInv = this->matrix[i][i].multiplicativeInvers();
				this->multiplyRow(i, mulInv);
				other.multiplyRow(i, mulInv);

				for (size_t j = 0; j < this->rows; j++) {
					if (i != j) {
						T addInv = this->matrix[j][i].additiveInvers();
						this->multiplyAndAddRows(i, j, addInv);
						other.multiplyAndAddRows(i, j, addInv);
					}
				}
			}
		} else {
			throw std::runtime_error("determinant == 0");
		}
	}

protected:
	T** matrix;
	size_t columns; // size-x
	size_t rows; // size-y

	virtual void multiplyRow(size_t index, T value) {
		if (index >= this->rows) {
			throw std::runtime_error("out of bounds");
		}

		for (size_t i = 0; i < columns; i++) {
			this->matrix[index][i] *= value;
		}
	}

	// add matrix[index1] onto matrix[index2]
	virtual void addRows(size_t index1, size_t index2) {
		if (index1 >= this->rows || index2 >= this->rows) {
			throw std::runtime_error("at least one index is out of bounds");
		}

		for (size_t i = 0; i < columns; i++) {
			this->matrix[index2][i] += this->matrix[index1][i];
		}
	}

	// add matrix[index1] * value onto matrix[index2]
	virtual void multiplyAndAddRows(size_t index1, size_t index2, T value) {
		if (index1 >= this->rows || index2 >= this->rows) {
			throw std::runtime_error("at least one index is out of bounds");
		}

		for (size_t i = 0; i < columns; i++) {
			this->matrix[index2][i] += this->matrix[index1][i] * value;
		}
	}

public:
	Matrix(size_t rows, size_t columns) {
		this->rows = rows;
		this->columns = columns;

		this->matrix = new T*[this->rows];
		for (size_t i = 0; i < this->rows; i++) {
			this->matrix[i] = new T[this->columns];
		}

		// TODO: is this always possible? No! we need an element to fill with
		this->fill(0);
	}

	Matrix(const Matrix& other) {
		this->copy(other);
	}

	Matrix(const Vector<T>& other) : Matrix(other.size(), other.size()) {
		for (int i = 0; i < other.size(); i++) {
			this->set(i, 0, other[i]);
		}
	}

	virtual ~Matrix() {
		this->clearData();
	}

	size_t getRows() const {
		return this->rows;
	}

	size_t getColumns() const {
		return this->columns;
	}

	void fill(T value) {
		for (size_t i = 0; i < this->rows; i++) {
			for (size_t j = 0; j < this->columns; j++) {
				this->matrix[i][j] = value;
			}
		}
	}

	void setToIdentityMatrix(T identity, T other) {
		if (this->rows != this->columns) {
			throw std::runtime_error("no identity matrix possible");
		}

		this->fill(other);

		for (size_t i = 0; i < this->rows; i++) {
			this->matrix[i][i] = identity;
		}
	}

	void set(size_t rowpos, size_t colpos, T value) {
		if (rowpos >= this->rows || colpos > this->columns) {
			throw std::runtime_error("rows or colums out of bounds");
		}
		this->matrix[rowpos][colpos] = value;
	}

	T get(size_t rowpos, size_t colpos) const {
		if (rowpos >= this->rows || colpos > this->columns) {
			throw std::runtime_error("rows or colums out of bounds");
		}
		return this->matrix[rowpos][colpos];
	}

	void flipRows(size_t index1, size_t index2) {
		if (index1 >= this->rows || index2 >= this->rows) {
			throw std::runtime_error("at least one index is to big");
		}

		T* temp = this->matrix[index1];
		this->matrix[index1] = this->matrix[index2];
		this->matrix[index2] = temp;
	}

	virtual T determinant() {
		if (this->rows != 3 || this->columns != 3) {
			throw std::runtime_error("this currently only works with a 3x3 matrix");
		}

		return (this->matrix[0][0] * this->matrix[1][1] * this->matrix[2][2])
				+ (this->matrix[0][1] * this->matrix[1][2] * this->matrix[2][0])
				+ (this->matrix[0][2] * this->matrix[1][0] * this->matrix[2][1])
				- (this->matrix[2][0] * this->matrix[1][1] * this->matrix[0][2])
				- (this->matrix[2][1] * this->matrix[1][2] * this->matrix[0][0])
				- (this->matrix[2][2] * this->matrix[1][0] * this->matrix[0][1]);
	}

	void transpose() {
		size_t newRows = this->columns;
		size_t newColumns = this->rows;
		T** newMatrix = new T*[newRows];

		for (size_t i = 0; i < newRows; i++) {
			newMatrix[i] = new T[newColumns];
			for (size_t j = 0; j < newColumns; j++) {
				newMatrix[i][j] = this->matrix[j][i];
			}
		}

		this->clearData();
		this->rows = newRows;
		this->columns = newColumns;
		this->matrix = newMatrix;
	}

	virtual void invert(T zeroElement, T oneElement) {
		if (this->determinant() > 0) {
			Matrix<T> invertedMatrix(this->rows, this->columns);
			invertedMatrix.setToIdentityMatrix(oneElement, zeroElement);

			this->gauss(invertedMatrix);

			// the original matrix should now be the identity

			// copy the elements to the current matrix
			this->copy(invertedMatrix);
		} else {
			throw std::runtime_error("determinant == 0");
		}
	}

	Matrix<T>& operator*=(const Matrix<T>& other) {
		if (this->columns != other.rows) {
			throw std::runtime_error("the matrixes cannot be multiplied");
		}

		Matrix<T> result(this->rows, other.columns);

		for (size_t i = 0; i < result.rows; i++) {
			for (size_t j = 0; j < result.columns; j++) {
				T temp;
				for (size_t k = 0; k < this->columns; k++) {
					temp += this->matrix[i][k] * other.matrix[k][j];
				}
				result.set(i, j, temp);
			}
		}

		// copy the result matrix to the current one
		this->clearData();
		this->copy(result);

		return *this;
	}

	Matrix<T> operator*(const Matrix<T>& other) const {
		Matrix<T> newMatrix(*this);
		return newMatrix *= other;
	}

	Vector<T> linearSystem(const Vector<T>& other) const {
		auto matrix = Matrix(*(this));
		auto resultMatrix = Matrix(other);
		if (matrix.rows == resultMatrix.rows) {
			matrix.gauss(resultMatrix);

			Vector<T> resultVector;
			for (int i = 0; i < matrix.rows; i++) {
				resultVector.emplace_back(resultMatrix.get(i, 0));
			}
			return resultVector;
		}
		throw std::runtime_error("vector has not the correct amount of elements");
	}
};

#endif // MATRIX_H
