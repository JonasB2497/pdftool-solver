#include "ellipticCurve.h"
#include "mathFunctions.h"
#include "zp.h"

#include <math.h>

EcPointZp::EcPointZp(EllipticCurveK3* parent) : EcPoint(Zp(0, parent->k), Zp(0, parent->k), parent) {
	this->isZeroPoint = true;
}

EcPointZp& EcPointZp::operator=(const EcPointZp& other) {
	this->x = other.x;
	this->y = other.y;
	this->parent = other.parent;
	this->isZeroPoint = other.isZeroPoint;
	return (*this);
}

EcPointZp& EcPointZp::operator+=(const EcPointZp& other) {
	if ((*this) == this->getZeroPoint()) {
		(*this) = other;
	}
	else if (other == this->getZeroPoint()) {
		// nothing to do here
	}
	else if ((this->x == other.x) && (this->y == -other.y)) {
		(*this) = this->getZeroPoint();
	}
	else if ((*this) == other && this->y != 0) {
		Zp newX = (this->x * this->x * 3) + this->parent->a;
		newX /= (this->y * 2);

		Zp newY = newX;

		newX *= newX;
		newX -= (this->x * 2);

		newY *= (this->x - newX);
		newY -= this->y;

		this->x = newX;
		this->y = newY;
	} else {
		// (x_1, y_1) + (x_2, y_2)
		Zp newX = (other.y - this->y) / (other.x - this->x);
		Zp newY = newX;

		newX *= newX;
		newX -= this->x;
		newX -= other.x;

		newY *= (this->x - newX);
		newY -= this->y;

		this->x = newX;
		this->y = newY;
	}

	return (*this);
}

EcPointZp EcPointZp::operator+(const EcPointZp& other) const {
	auto newPoint((*this));
	return newPoint += other;
}

bool EcPointZp::operator==(const EcPointZp& other) const {
	if (this->parent == nullptr) {
		return (this->x == other.x) && (this->y == other.y) && (this->isZeroPoint == other.isZeroPoint);
	}
	return (this->x == other.x) && (this->y == other.y) && (this->isZeroPoint == other.isZeroPoint) && (this->parent == other.parent);
}

bool EcPointZp::operator!=(const EcPointZp& other) const {
	return !((*this) == other);
}



EcPointGF2n::EcPointGF2n(EllipticCurveK2* parent) : EcPoint(GF2n(0, parent->k, parent->irreducible), GF2n(0, parent->k, parent->irreducible), parent) {
	this->isZeroPoint = true;
}

EcPointGF2n& EcPointGF2n::operator=(const EcPointGF2n& other) {
	this->x = other.x;
	this->y = other.y;
	this->parent = other.parent;
	this->isZeroPoint = other.isZeroPoint;
	return (*this);
}

EcPointGF2n& EcPointGF2n::operator+=(const EcPointGF2n& other) {
	if (this->isZeroPoint) {
		(*this) = other;
	}
	else if (other.isZeroPoint) {
		// nothing to do here
	}
	else if ((this->x == other.x) && ((this->x + this->y) == other.y)) {
		(*this) = this->getZeroPoint();
	} else {
		// (x_1, y_1) + (x_2, y_2)
		GF2n u;
		if (this->x == other.x) {
			u = this->x + (this->y / this->x);
		} else {
			u = (this->y + other.y) / (this->x + other.x);
		}
		GF2n newX = u * u + u + this->x + other.x + this->parent->a;

		GF2n newY = newX + this->x;
		newY *= u;
		newY += newX;
		newY += this->y;

		this->x = newX;
		this->y = newY;
	}

	return (*this);
}

EcPointGF2n EcPointGF2n::operator+(const EcPointGF2n& other) const {
	auto newPoint((*this));
	return newPoint += other;
}

bool EcPointGF2n::operator==(const EcPointGF2n& other) const {
	if (this->parent == nullptr) {
		return (this->x == other.x) && (this->y == other.y) && (this->isZeroPoint == other.isZeroPoint);
	}
	return (this->x == other.x) && (this->y == other.y) && (this->isZeroPoint == other.isZeroPoint) && (this->parent == other.parent);
}

bool EcPointGF2n::operator!=(const EcPointGF2n& other) const {
	return !((*this) == other);
}






EllipticCurveK3::EllipticCurveK3(DATATYPE k, DATATYPE a, DATATYPE b) : EllipticCurve(k, Zp(a, k), Zp(b, k)) {
	calculatePoints();
}

void EllipticCurveK3::calculatePoints() {
	this->points.emplace_back(EcPointZp(this));
	for (DATATYPE i = 0; i < this->k; i++) {
		for (DATATYPE j = 0; j < this->k; j++) {
			auto result = Zp(quickpot(i, 3, this->k), this->k) + Zp(i, this->k) * this->a + this->b;

			if (result == quickpot(j, 2, this->k)) {
				this->points.emplace_back(EcPointZp(Zp(i, this->k), Zp(j, this->k), this));
			}
		}
	}
}



EllipticCurveK2::EllipticCurveK2(DATATYPE k, DATATYPE irreducible, GF2n a, GF2n b) : irreducible(irreducible), EllipticCurve(k, a, b) {
	calculatePoints();
}

void EllipticCurveK2::calculatePoints() {
	this->points.emplace_back(EcPointGF2n(this));
	long n = pow(2, this->k);
	for (DATATYPE i = 0; i < n; i++) {
		GF2n gfi = GF2n(i, this->k, this->irreducible);
		for (DATATYPE j = 0; j < n; j++) {
			GF2n gfj = GF2n(j, this->k, this->irreducible);
			auto result1 = gfj * gfj + gfi * gfj;
			auto result2 = gfi * gfi * gfi + this->a * (gfi * gfi) + this->b;

			if (result1 == result2) {
				this->points.emplace_back(EcPointGF2n(gfi, gfj, this));
			}
		}
	}
}
