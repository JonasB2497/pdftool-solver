#include "mathFunctions.h"
#include "zp.h"
#include "polynom.h"
#include "utility.h"

#include "spdlog/spdlog.h"
#include "spdlog/fmt/ostr.h"
#include "spdlog/fmt/bundled/ranges.h"

#include <stdexcept>
#include <cstdarg>
#include <math.h>
#include <sstream>

long quickpot(long a, long b, long n) {
	long help;
	if (b==0) {return 1;}
	help=quickpot(a,b/2,n);
	help=(help*help)%n;
	if (b%2==1) {help=(help*a)%n;}
	return help;
}

long order(long a, long n) {
	for (long i = 1; i <= n; i++) {
		if (quickpot(a, i, n) == 1) {
			return i;
		}
	}
	throw std::runtime_error("number has no order");
}

template<>
long ggt(long a, long b) {
	int h;
	if (a == 0) return abs(b);
	if (b == 0) return abs(a);

	do {
		h = a % b;
		a = b;
		b = h;
	} while (b != 0);
	return abs(a);
}

template<>
Polynom<Zp> ggt(Polynom<Zp> a, Polynom<Zp> b) {
	Polynom<Zp> h;
	Polynom<Zp> multi;
	if (a.getDegree() == 0) return b;
	if (b.getDegree() == 0) return a;

	do {
		if (a >= b) {
			multi = Polynom<Zp>(a.getDegree() - b.getDegree());
			multi.setMultiplicatorAt(multi.getDegree(), a.getMultiplicatorAt(a.getDegree()) / b.getMultiplicatorAt(b.getDegree()));
		} else {
			multi = Polynom<Zp>({0});
		}
		h = a - (multi * b);
		spdlog::debug("({}) = ({}) * ({}) + ({})", a, multi, b, h);
		a = b;
		b = h;
	} while (!(b.getDegree() == 0 && b.getMultiplicatorAt(0) == 0));
	return a;
}

long phi(long n) {
	long result = 0;
	for (long i = 1; i <= n; i++) {
		if (ggt(n, i) == 1) {
			result++;
		}
	}
	return result;
}

bool isPrimitiveRoot(long a, long n) {
	return order(a, n) == phi(n);
}


template<>
EuklidResults<long> euklid(long m, long n) {
	long v;
	long a[3];
	long c[3];
	long d[3];

	c[0] = 1;
	c[1] = 0;
	d[0] = 0;
	d[1] = 1;
	a[0] = n;
	a[1] = m;

	while (a[1] != 0)
	{
		a[2] = a[0] % a[1];
		v = a[0] / a[1];
		c[2] = c[0] - v * c[1];
		d[2] = d[0] - v * d[1];

		spdlog::debug("{} = {} * {} + {}    -->    {} = 1 * {} + ({}) * {}    -->    {} = {} * {} + {} * {}",
			a[0], v, a[1], a[2],
			a[2], a[0], -v, a[1],
			a[2], c[2], n, d[2], m);

		a[0] = a[1];
		a[1] = a[2];
		c[0] = c[1];
		c[1] = c[2];
		d[0] = d[1];
		d[1] = d[2];
	}
	while (c[0]<0) {c[0] = c[0] + m;}
	while (d[0]<0) {d[0] = d[0] + n;}
	return EuklidResults<long>({a[0], c[0], d[0]});
}

/*template<>
EuklidResults<Polynom<Zp>> euklid(Polynom<Zp> m, Polynom<Zp> n) {
	int i = 0;
	Polynom<Zp> v;
	Polynom<Zp> a[3];
	Polynom<Zp> c[3];
	Polynom<Zp> d[3];

	c[0] = Polynom<Zp>({Zp(1)});
	c[1] = Polynom<Zp>({Zp(0)});
	d[0] = Polynom<Zp>({Zp(0)});
	d[1] = Polynom<Zp>({Zp(1)});
	a[0] = m;
	a[1] = n;

	while (!(a[1].getDegree() == 0 && a[1].getMultiplicatorAt(0) == 0))
	{
		// calculate a2 and v for euklid
		if (a[0].getDegree() >= a[1].getDegree()) {
			v = Polynom<Zp>(a[0].getDegree() - a[1].getDegree());
			v.setMultiplicatorAt(v.getDegree(), a[0].getMultiplicatorAt(a[0].getDegree()) / a[1].getMultiplicatorAt(a[1].getDegree()));
		} else {
			v = Polynom<Zp>({0});
		}
		a[2] = a[0] - (v * a[1]);
		//spdlog::debug("a{} = {}; v{} = {}", i, a[0], i+1, v);
		spdlog::debug("({}) = ({}) * ({}) + ({})", a[0], v, a[1], a[2]);

		// calculate c and d for extended euklid
		c[2] = c[0] - v * c[1];
		d[2] = d[0] - v * d[1];
		// TODO: this output might be needed in an other way (more like in the book)
		//spdlog::debug("\tc{} = {}; d{} = {}", i+1, c[2], i+1, d[2]);
		spdlog::debug("\t({}) = ({}) * ({}) + ({}) * ({})", a[2], 1, a[0], -v, a[1]);
		spdlog::debug("\t({}) = ({}) * ({}) + ({}) * ({})", a[2], c[2], m, d[2], n);
		i++;

		//move every element one index
		a[0] = a[1];
		a[1] = a[2];
		c[0] = c[1];
		c[1] = c[2];
		d[0] = d[1];
		d[1] = d[2];
	}
	return EuklidResults<Polynom<Zp>>({a[0], c[0], d[0]});
}*/

template<>
EuklidResults<Polynom<Zp>> euklid(Polynom<Zp> m, Polynom<Zp> n) {
	int i = 0;
	Polynom<Zp> v;
	Polynom<Zp> a[3];
	Polynom<Zp> c[3];
	Polynom<Zp> d[3];

	c[0] = Polynom<Zp>({Zp(1)});
	c[1] = Polynom<Zp>({Zp(0)});
	d[0] = Polynom<Zp>({Zp(0)});
	d[1] = Polynom<Zp>({Zp(1)});
	a[0] = m;
	a[1] = n;

	while (!(a[1].getDegree() == 0 && a[1].getMultiplicatorAt(0) == 0))
	{
		// calculate a2 and v for euklid
		if (a[0].getDegree() >= a[1].getDegree()) {
			v = a[0] / a[1];
			//a[2] = v.getRemainder();
			a[2] = a[0] % a[1];
		} else {
			v = Polynom<Zp>({0});
			a[2] = a[0];
		}
		//spdlog::debug("a{} = {}; v{} = {}", i, a[0], i+1, v);
		spdlog::debug("({}) = ({}) * ({}) + ({})", a[0], v, a[1], a[2]);

		// calculate c and d for extended euklid
		c[2] = c[0] - v * c[1];
		d[2] = d[0] - v * d[1];
		// TODO: this output might be needed in an other way (more like in the book)
		//spdlog::debug("\tc{} = {}; d{} = {}", i+1, c[2], i+1, d[2]);
		spdlog::debug("\t({}) = ({}) * ({}) + ({}) * ({})", a[2], 1, a[0], -v, a[1]);
		spdlog::debug("\t({}) = ({}) * ({}) + ({}) * ({})", a[2], c[2], m, d[2], n);
		i++;

		//move every element one index
		a[0] = a[1];
		a[1] = a[2];
		c[0] = c[1];
		c[1] = c[2];
		d[0] = d[1];
		d[1] = d[2];
	}
	return EuklidResults<Polynom<Zp>>({a[0], c[0], d[0]});
}

long chineseRemainderTheorem(long* a, long* m, size_t len) {
	// multiply all m's
	long mn = 1;
	for (size_t i = 0; i < len; i++) {
		mn *= m[i];
	}

	// calculate all M's (M_i = m / m_i)
	long bigM[len];
	for (size_t i = 0; i < len; i++) {
		bigM[i] = mn / m[i];
	}
	spdlog::debug("bigM: {}", std::vector<long>(bigM, bigM + len));

	Zp x(0, mn);
	std::stringstream debug;
	for (size_t i = 0; i < len; i++) {
		x += (a[i] * Zp(bigM[i], m[i]).multiplicativeInvers().getValue() * bigM[i]);
		debug << Zp(bigM[i], m[i]).multiplicativeInvers().getValue() << ", ";
	}
	spdlog::debug("y: [{}]", debug.str());

	return x.getValue();
}
