#ifndef ELLIPTICCURVE_H
#define ELLIPTICCURVE_H

#include "zp.h"
#include "gf2n.h"

#include <vector>

#define DATATYPE long

class EllipticCurveK3;
class EllipticCurveK2;

template<typename T, typename P, typename A>
class EcPoint {
public:
	T x;
	T y;
	bool isZeroPoint;
	P* parent;

	A getZeroPoint() { return A(this->parent); }

	EcPoint(T x, T y, P* parent) : x(x), y(y), parent(parent), isZeroPoint(false) {}
	EcPoint(const EcPoint<T, P, A>& obj) : x(obj.x), y(obj.y), parent(obj.parent), isZeroPoint(obj.isZeroPoint) {}
	EcPoint(P* parent) : x(0), y(0), parent(parent), isZeroPoint(true) {}

	virtual A& operator=(const A& other) = 0;
	virtual A& operator+=(const A& other) = 0;
	virtual A operator+(const A& other) const = 0;
	virtual bool operator==(const A& other) const = 0;
	virtual bool operator!=(const A& other) const = 0;
};

class EcPointZp : public EcPoint<Zp, EllipticCurveK3, EcPointZp> {
public:
	using EcPoint<Zp, EllipticCurveK3, EcPointZp>::EcPoint;
	EcPointZp(EllipticCurveK3* parent);

	EcPointZp& operator=(const EcPointZp& other) override;
	EcPointZp& operator+=(const EcPointZp& other) override;
	EcPointZp operator+(const EcPointZp& other) const override;

	bool operator==(const EcPointZp& other) const override;
	bool operator!=(const EcPointZp& other) const override;
};

class EcPointGF2n : public EcPoint<GF2n, EllipticCurveK2, EcPointGF2n> {
public:
	using EcPoint<GF2n, EllipticCurveK2, EcPointGF2n>::EcPoint;
	EcPointGF2n(EllipticCurveK2* parent);


	EcPointGF2n& operator=(const EcPointGF2n& other) override;
	EcPointGF2n& operator+=(const EcPointGF2n& other) override;
	EcPointGF2n operator+(const EcPointGF2n& other) const override;

	bool operator==(const EcPointGF2n& other) const override;
	bool operator!=(const EcPointGF2n& other) const override;
};




template<typename T, typename P>
class EllipticCurve {
protected:
	T a;
	T b;
	DATATYPE k;
	std::vector<P> points;
	virtual void calculatePoints() = 0;

public:
	EllipticCurve(DATATYPE k, T a, T b) : k(k), a(a), b(b) {}
	const std::vector<P> getPoints() const { return this->points; }
	DATATYPE getK() { return this->k; }
};



class EllipticCurveK3 : public EllipticCurve<Zp, EcPointZp> {
friend EcPointZp;
protected:
	void calculatePoints() override;

public:
	EllipticCurveK3(DATATYPE k, DATATYPE a, DATATYPE b);
};



class EllipticCurveK2 : public EllipticCurve<GF2n, EcPointGF2n> {
friend EcPointGF2n;
protected:
	DATATYPE irreducible;
	void calculatePoints() override;

public:
	EllipticCurveK2(DATATYPE k, DATATYPE irreducible, GF2n a, GF2n b);
};

#endif
