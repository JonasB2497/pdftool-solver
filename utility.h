#ifndef UTILITY_H
#define UTILITY_H

#include "ellipticCurve.h"
#include "polynom.h"
#include "matrix.h"
#include "spdlog/spdlog.h"
#include <ostream>
#include <iostream>
#include <iomanip>
#include <string>

long binToDec(long bin);
long decToBin(long dec);

std::ostream& operator<<(std::ostream& os, const GF2n value);
std::ostream& operator<<(std::ostream& os, const Zp value);

template <typename T, typename P, typename A>
std::ostream& operator<<(std::ostream& os, const EcPoint<T, P, A>& pt) {
	if (pt.isZeroPoint) {
		std::string zero = "∅";
		os << "(";
		os << std::setfill(' ') << std::setw(pt.parent->getK() + zero.length() - 1) << zero;
		os << ",";
		os << std::setfill(' ') << std::setw(pt.parent->getK() + zero.length() - 1) << zero;
		os << ")";

	} else {
		os << "(" << pt.x << "," << pt.y << ")";
	}
	return os;
}

template<typename T>
std::ostream& operator<<(std::ostream& os, const Polynom<T>& polynom) {
	for (long i = polynom.getDegree(); i >= 0; i--) {
		os << polynom.getMultiplicatorAt(i);
		if (i > 0) {
			os << "X";
			if (i > 1) {
				os << "^" << i;
			}
		}

		if (i != 0) {
			os << " + ";
		}
	}

	if (polynom.hasRemainder()) {
		os << " remainder: " << polynom.getRemainder();
	}

	return os;
}

template<typename T>
std::ostream& operator<<(std::ostream& os, const Matrix<T>& matrix) {
	for (size_t i = 0; i < matrix.getRows(); i++) {
		os << "\t";
		for (size_t j = 0; j < matrix.getColumns(); j++) {
			os << matrix.get(i, j) << ", ";
		}
		os << std::endl;
	}
	return os;
}

template<typename T>
std::ostream& operator<<(std::ostream& os, const Vector<T>& vector) {
	for (size_t i = 0; i < vector.size(); i++) {
		os << vector.at(i) << ", ";
	}
	os << std::endl;
	return os;
}

template<typename T, typename P>
void printEcPlusTable(const EllipticCurve<T, P>& ec) {
	auto level = spdlog::get_level();
	spdlog::set_level(spdlog::level::off);

	// print the header
	std::cout << "+ | ";
	for (size_t i = 0; i < ec.getPoints().size(); i++) {
		std::cout << ec.getPoints()[i] << " ";
	}
	std::cout << std::endl;

	for (size_t i = 0; i < ec.getPoints().size(); i++) {
		std::cout << ec.getPoints()[i] << " | ";
		for (size_t j = 0; j < ec.getPoints().size(); j++) {
			std::cout << (ec.getPoints()[i] + ec.getPoints()[j]) << " ";
		}
		std::cout << std::endl;
	}

	spdlog::set_level(level);
}

#endif // UTILITY_H
